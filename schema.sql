-- STT-DB 
--
-- Database for storing music and user data
--
-- Schema init script, to be read by sqlite3
-- Datatypes at https://www.sqlite.org/datatype3.html
-- Special commands at https://www.sqlite.org/cli.html

-- Clear any existing tables
DROP TABLE IF EXISTS Artist;
DROP TABLE IF EXISTS Label;
DROP TABLE IF EXISTS Studio;
DROP TABLE IF EXISTS Album;
DROP TABLE IF EXISTS Track;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS User_Track_Rating;
DROP TABLE IF EXISTS Playlist;
DROP TABLE IF EXISTS Playlist_Track;
DROP TABLE IF EXISTS Recording_File;
DROP TABLE IF EXISTS Track_File;

-- Create new tables
CREATE TABLE Artist (
    artist_id INTEGER PRIMARY KEY,
    artist_name text NOT NULL,
    bio text,
    dob date,
    --date of death
    dod date

    -- PRIMARY KEY(artist_id)
    -- Moved INTEGER PRIMARY KEY inline to allow autoincrement
    -- https://sqlite.org/autoinc.html
);

INSERT INTO Artist VALUES(4,'Amorphis',NULL,NULL,NULL);

CREATE TABLE Label (
    label_id INTEGER PRIMARY KEY,
    label_name text

    -- PRIMARY KEY(label_id)
);


CREATE TABLE Studio (
    studio_id INTEGER PRIMARY KEY,
    studio_name text
    
    -- PRIMARY KEY(studio_id)
);

CREATE TABLE Album (
    album_id INTEGER PRIMARY KEY,
    title text,
    catalog_num int,
    a_date date,
    a_length int,
    track_list text,
    cover_art text,
    -- foreign
    artist_id int,
    label_id int,
    studio_id int,

    -- keys
    -- PRIMARY KEY (album_id),
    FOREIGN KEY (artist_id) REFERENCES Artist(artist_id),
    FOREIGN KEY (label_id) REFERENCES Label(label_id),
    FOREIGN KEY (studio_id) REFERENCES Studio(studio_id)
);

INSERT INTO Album VALUES(4,'Under The Red Cloud',NULL,NULL,3599,NULL,'albums/Under The Red Cloud/album.jpg',4,NULL,NULL);

CREATE TABLE Track (
    track_id INTEGER PRIMARY KEY,
    track_number int,
    track_name text,
    track_length int,
    recorded_by text,
    lyrics text,
    --foreign
    album_id int,

    --key
    -- PRIMARY KEY (track_id),
    FOREIGN KEY (album_id) REFERENCES Album(album_id)
);

INSERT INTO Track VALUES(106150330,1,'Under The Red Cloud',333,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150332,2,'The Four Wise Ones',280,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150334,3,'Bad Blood',323,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150336,4,'The Skull',304,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150338,5,'Death Of A King',314,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150340,6,'Sacrifice',236,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150342,7,'Dark Path',308,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150344,8,'Enemy At The Gates',307,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150346,9,'Tree Of Ages',276,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150348,10,'White Night',315,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150350,11,'Come The Spring (Bonus Track)',207,'Amorphis',NULL,4);
INSERT INTO Track VALUES(106150352,12,'Winter''s Sleep (Bonus Track)',396,'Amorphis',NULL,4);

CREATE TABLE User (
    u_id INTEGER PRIMARY KEY,
    u_name text,
    --foreign
    top_album_id int,
    top_track_id int,

    --key
    -- PRIMARY KEY (u_id),
    FOREIGN KEY (top_album_id) REFERENCES Album(album_id),
    FOREIGN KEY (top_track_id) REFERENCES Track(track_id)
);

-- INSERT INTO User (u_name) VALUES('Max');
INSERT INTO User VALUES(1,'Max',NULL,NULL);
INSERT INTO User VALUES(2,'Kyle',NULL,NULL);

CREATE TABLE User_Track_Rating (
    rating int, -- 0 is NULL, 1 is min rank, 5 is max
    plays int,
    --foreign
    track_id int,
    u_id int,

    --key
    PRIMARY KEY(u_id, track_id),
    FOREIGN KEY (track_id) REFERENCES Track(track_id),
    FOREIGN KEY (u_id) REFERENCES User(u_id)
);

INSERT INTO User_Track_Rating VALUES(5,30,106150330,1);
INSERT INTO User_Track_Rating VALUES(1,6969,106150340,1);

CREATE TABLE Playlist (
    playlist_id INTEGER PRIMARY KEY,
    playlist_name text,
    --foreign
    u_id int,
    --key
    FOREIGN KEY (u_id) REFERENCES User(u_id)
);

INSERT INTO Playlist VALUES(1,'My Playlist 1', 1);

CREATE TABLE Playlist_Track (
    time_added timestamp, --epoch timestamp
    --foreign
    playlist_id int,
    track_id int,
    --key
    PRIMARY KEY(time_added, playlist_id, track_id),
    FOREIGN KEY (playlist_id) REFERENCES Playlist(playlist_id),
    FOREIGN KEY (track_id) REFERENCES Track(track_id)
);

INSERT INTO Playlist_Track VALUES("2022-05-20T17:20:14.060832",1,106150340);
INSERT INTO Playlist_Track VALUES("2022-05-20T17:20:15.060832",1,106150340);
INSERT INTO Playlist_Track VALUES("2022-05-20T17:20:16.060832",1,106150340);
INSERT INTO Playlist_Track VALUES("2022-05-20T17:20:17.060832",1,106150348);
INSERT INTO Playlist_Track VALUES("2022-05-20T17:20:18.060832",1,106150340);
INSERT INTO Playlist_Track VALUES("2022-05-20T17:20:19.060832",1,106150340);

CREATE TABLE Recording_File (
    recording_id INTEGER PRIMARY KEY,
    f_name text,
    rec_length int,
    disc int,
    side char(1),
    --foreign
    u_id int,
    album_id int,

    --key
    -- PRIMARY KEY(recording_id),
    FOREIGN KEY (u_id) REFERENCES User(u_id),
    FOREIGN KEY (album_id) REFERENCES Album(album_id)
);

INSERT INTO Recording_File VALUES(1,'Amorphis_test.wav',3605,1,'A',NULL,4);
INSERT INTO Recording_File VALUES(2,'S̲corpions̲ – Blacko̲u̲t̲ Full Album 1982 [npZt26YwbQc].wav',2208,1,'A',NULL,NULL);
INSERT INTO Recording_File VALUES(3,'Bonobo - Fragments (Full Album) 2022 [B1lAX8PFPO8].wav',3082,1,'A',NULL,NULL);
INSERT INTO Recording_File VALUES(4,'Daft Punk - Random Access Memories (Full Album) [wIMSU8otS-g].wav',4476,1,'A',NULL,NULL);
INSERT INTO Recording_File VALUES(5,'Joji - Nectar (Full Album) [5eDk-kTE9DI].wav',3195,1,'A',NULL,NULL);
INSERT INTO Recording_File VALUES(6,'Gregory Alan Isakov - Evening Machines [Full Album] [sMX2SCP6tzQ].wav',2624,1,'A',NULL,NULL);


CREATE TABLE Track_File (
    track_file_id INTEGER PRIMARY KEY,
    f_name text,
    start_time int,
    stop_time int,
    --foreign
    track_id int,
    u_id int,
    recording_id int,

    --key
    -- PRIMARY KEY (track_file_id),
    FOREIGN KEY (track_id) REFERENCES Track(track_id),
    FOREIGN KEY (u_id) REFERENCES User(u_id),
    FOREIGN KEY (recording_id) REFERENCES Recording_File(recording_id)
);

INSERT INTO Track_File VALUES(106150330,'albums/Under The Red Cloud/Under The Red Cloud.flac',0,330000,106150330,NULL,1);
INSERT INTO Track_File VALUES(106150332,'albums/Under The Red Cloud/The Four Wise Ones.flac',333000,610000,106150332,NULL,1);
INSERT INTO Track_File VALUES(106150334,'albums/Under The Red Cloud/Bad Blood.flac',613000,936000,106150334,NULL,1);
INSERT INTO Track_File VALUES(106150336,'albums/Under The Red Cloud/The Skull.flac',936000,1240000,106150336,NULL,1);
INSERT INTO Track_File VALUES(106150338,'albums/Under The Red Cloud/Death Of A King.flac',1240000,1554000,106150338,NULL,1);
INSERT INTO Track_File VALUES(106150340,'albums/Under The Red Cloud/Sacrifice.flac',1554000,1790000,106150340,NULL,1);
INSERT INTO Track_File VALUES(106150342,'albums/Under The Red Cloud/Dark Path.flac',1790000,2098000,106150342,NULL,1);
INSERT INTO Track_File VALUES(106150344,'albums/Under The Red Cloud/Enemy At The Gates.flac',2098000,2405000,106150344,NULL,1);
INSERT INTO Track_File VALUES(106150346,'albums/Under The Red Cloud/Tree Of Ages.flac',2405000,2681000,106150346,NULL,1);
INSERT INTO Track_File VALUES(106150348,'albums/Under The Red Cloud/White Night.flac',2681000,2996000,106150348,NULL,1);
INSERT INTO Track_File VALUES(106150350,'albums/Under The Red Cloud/Come The Spring (Bonus Track).flac',2996000,3203000,106150350,NULL,1);
INSERT INTO Track_File VALUES(106150352,'albums/Under The Red Cloud/Winter''s Sleep (Bonus Track).flac',3203000,3599000,106150352,NULL,1);

-- Load Data?
.mode csv
-- .import ./data/artist.csv Artist
-- .import ./data/label.csv Label
-- .import ./data/studio.csv Studio
-- .import ./data/album.csv Album
-- .import ./data/track.csv Track
-- .import ./data/user.csv User
-- .import ./data/rating.csv Rating
-- .import ./data/recording_file.csv Recording_File
-- .import ./data/track_file.csv Track_File

.mode column
