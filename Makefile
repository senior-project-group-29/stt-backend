# Makefile
#
# 

.PHONY: restart db start stop

dev:
	python3 ./server/server.py

activate:
	source env/bin/activate

dumpdb: stt.db
	sqlite3 stt.db .dump > dump.sql

db: stt.db
	sqlite3 stt.db

# make stt.db -B
# -B forces rebuild
stt.db:
	sqlite3 -init schema.sql stt.db

# for prod only

stop:
	mv uwsgi.ini uwsgi.ini.bak

start:
	mv uwsgi.ini.bak uwsgi.ini

restart: uwsgi.ini
	mv uwsgi.ini uwsgi.ini.bak
	sleep 15
	mv uwsgi.ini.bak uwsgi.ini
	sleep 5
	touch uwsgi.ini
