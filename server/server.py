import sqlite3
import os.path
import json
from datetime import datetime

from audio.record_and_identify import record_and_identify
from flask import Blueprint, Flask, jsonify, request, render_template, send_from_directory

app = Flask(__name__, static_folder="../app", static_url_path='/app')
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ART_PATH = 'art'
DB_PATH = os.path.join(BASE_DIR, 'stt.db')
# print(f"__________ PATH ___________ {DB_PATH}")


def __dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        # col[0] is the column name
        d[col[0]] = row[idx]
    return d

def __jsonFromSql1(sqlStr):
    con = sqlite3.connect(DB_PATH)
    con.row_factory = __dict_factory
    c = con.cursor()
    c.execute(sqlStr)
    rst = c.fetchall()  # rst is a list of dict
    con.close()
    return jsonify(rst)

def __jsonFromSql(sqlStr, argTup):
    con = sqlite3.connect(DB_PATH)
    con.row_factory = __dict_factory
    c = con.cursor()
    c.execute(sqlStr,argTup)
    rst = c.fetchall()  # rst is a list of dict
    con.close()
    return jsonify(rst)

# All Users
@app.route('/api/all_users')
def getAllUsers():
    sqlStr = 'SELECT * FROM User'
    return __jsonFromSql1(sqlStr)

# All Artists
@app.route('/api/all_artists')
def getArtistData():
    sqlStr = 'SELECT * FROM Artist'
    return __jsonFromSql1(sqlStr)

# All Recordings
@app.route('/api/all_recordings')
def getAllRecordings():
    sqlStr = 'SELECT * FROM Recording_File'
    return __jsonFromSql1(sqlStr)

# All Playlists
@app.route('/api/all_playlists')
def getAllPlaylists():
    sqlStr = 'SELECT * FROM Playlist'
    return __jsonFromSql1(sqlStr)

# All Albums
@app.route('/api/album')
def getAlbumData():
    sqlStr = 'SELECT * FROM Album R JOIN Artist A on A.artist_id=R.artist_id'
    return __jsonFromSql1(sqlStr)

# User Playlists
@app.route('/api/user_playlists')
def getUsersPlaylists():
    arg = request.args.get("uid")
    sqlStr = 'SELECT * FROM Playlist WHERE u_id=?'
    return __jsonFromSql(sqlStr, (arg,))

# Individual Recording+Album for a Recording_ID 
# /api/recording?id=4
@app.route('/api/recording')
def getRecording():
    table = "Recording_File"
    arg = request.args.get("id")

    print(f"Request for {request.path}, {request.args}")

    con = sqlite3.connect(DB_PATH)
    con.row_factory = __dict_factory
    c = con.cursor()
    c.execute(
        f'SELECT * \
        FROM "{table}" R \
        JOIN Album A on A.album_id=R.album_id \
        WHERE R.recording_id=?', 
        (arg,)
        )
    rst = c.fetchall()  # rst is a list of dict
    con.close()
    return jsonify(rst)



# All Tracks for an Album
# /api/track?id=4
@app.route('/api/track')
def getTrackDataByAlbumID():
    table = "Track"
    arg = request.args.get("id")
    sqlStr = f'SELECT * FROM Track T \
        JOIN Track_File F ON T.track_id=F.track_id \
        WHERE album_id=?'
    argTup = (arg,)
    print(f"Request for {request.path}, {request.args}")

    return __jsonFromSql(sqlStr, argTup)

# All Tracks for a Playlist
# /api/all_playlist_tracks?id=4
@app.route('/api/all_playlist_tracks')
def getAllPlaylistTracks():
    arg = request.args.get("id")
    sqlStr = f'SELECT * \
        FROM Playlist_Track P \
        JOIN Track T on P.track_id=T.track_id \
        JOIN Track_File F on P.track_id=F.track_id \
        JOIN Album A on T.album_id=A.album_id \
        JOIN Artist R on A.artist_id=R.artist_id \
        WHERE playlist_id=?'
    argTup = (arg,)

    print(f"Request for {request.path}, {request.args}")

    return __jsonFromSql(sqlStr, argTup)

# All Tracks Ratings for a User
# /api/all_user_track_ratings?id=1
@app.route('/api/all_user_track_ratings')
def getAllUserTrackRatings():
    arg = request.args.get("id")
    sqlStr = f'SELECT * \
        FROM User_Track_Rating U \
        JOIN Track T on U.track_id=T.track_id \
        JOIN Album A on T.album_id=A.album_id \
        JOIN Artist R on A.artist_id=R.artist_id \
        WHERE u_id=?'
    argTup = (arg,)

    print(f"Request for {request.path}, {request.args}")

    return __jsonFromSql(sqlStr, argTup)

# All Tracks_Files for a Recording_File
# /api/all_track_files?id=4
@app.route('/api/all_track_files')
def getTrackFilesByRecordingID():
    arg = request.args.get("id")
    sqlStr = f'SELECT * FROM Track_File WHERE recording_id=?'
    argTup = (arg,)

    print(f"Request for {request.path}, {request.args}")

    return __jsonFromSql(sqlStr, argTup)

# Artist and Album name from Album ID
@app.route('/api/album_info')
def getAlbumInfo():
    arg = request.args.get("id")
    sqlStr = f'SELECT * FROM Album A \
        JOIN Artist R ON A.artist_id=R.artist_id \
        WHERE A.album_id=?'
    argTup = (arg,)

    print(f"Request for {request.path}, {request.args}")

    return __jsonFromSql(sqlStr, argTup)

# Individual Album art for an Album
@app.route('/api/art')
def getArt():
    table = "Album"
    arg = request.args.get("id")
    print(f"Request for {request.path}, {request.args}")

    con = sqlite3.connect(DB_PATH)
    # con.row_factory = __dict_factory
    # con.row_factory = sqlite3.Row
    c = con.cursor()
    c.execute(f'SELECT cover_art FROM "{table}" WHERE album_id=?', (arg,))
    rst = c.fetchone()[0]  # rst is a string path to the cover art
    file = os.path.join(ART_PATH, rst)
    print(f"Got Result: {file}")
    con.close()
    # WARNING: send_static_file can be dangerous.
    # Here we make sure that our path only comes from the ART_PATH
    # and the filename comes directly out of the database
    # https://stackoverflow.com/a/28758723/12670653
    return app.send_static_file(file)

# BACKEND CODE

def commitToDB(sqlStr, argTup):
    ret = ('', 205)
    try:
        con = sqlite3.connect(DB_PATH)
        c = con.cursor()
        c.execute(sqlStr, argTup)
        con.commit()
        con.close()
        return ('', 205)
    except:
        print("Error: commitToDB()")
        return ('', 505)

# /api/listened?id=106150330&u=1
@app.route('/api/listened')
def incUserTrackListens():
    print(f"Request for {request.path}, {request.args}")
    t_id = request.args.get('id')
    u_id = request.args.get('u')
    argTup = (t_id, u_id)
    sqlStr = 'SELECT plays FROM User_Track_Rating \
        WHERE track_id=? AND u_id=?'

    try:
        con = sqlite3.connect(DB_PATH)
        c = con.cursor()
        c.execute(sqlStr, argTup)
        plays = c.fetchone()
        # print("plays, ", plays)
        if plays:
            plays = plays[0]
            plays += 1
            
            argTup = (plays, t_id, u_id)
            sqlStr = 'UPDATE User_Track_Rating \
                SET plays=? \
                WHERE track_id=? AND u_id=?'

            c.execute(sqlStr, argTup)
            con.commit()
            con.close()
            print('Incremented User_Track_Rating.plays to', plays)
            return ('', 205)
        else:
            plays = 1
            argTup = (plays, t_id, u_id)
            sqlStr = 'INSERT INTO User_Track_Rating \
                (plays, track_id, u_id)  \
                VALUES (?, ?, ?)'

            c.execute(sqlStr, argTup)
            con.commit()
            con.close()
            print('Inserted new User_Track_Rating')
            return ('', 205)
    except:
        print("Error: incUserTrackListens()")
        return ('', 505)

# /api/split?id=1
@app.route('/api/split')
def getSplit():
    table = "Recording_File"
    arg = request.args.get("id")
    print(f"Request for {request.path}, {request.args}")

    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    c.execute(f'SELECT f_name FROM "{table}" WHERE recording_id=?', (arg,))
    rst = c.fetchone()[0]  # rst is a string

    rai = record_and_identify(rst, arg)
    rai.identify()
    rai.split()

    con.close()
    return ('', 204)

# /api/adjustTrack?tfid=1&start=0&stop=333000
@app.route('/api/adjustTrack')
def adjustTrackWithStartStop():
    tfID = int(request.args.get("tfid"))
    start = int(request.args.get("start"))
    stop = int(request.args.get("stop"))

    con = sqlite3.connect(DB_PATH)
    c = con.cursor()

    # c.execute(f'SELECT f_name FROM Track_File WHERE track_id=?', [tfID])
    # trackPath = c.fetchone()[0]  # rst is a string

    c.execute(f'SELECT R.f_name, R.recording_id, T.f_name \
        FROM Recording_File R \
        JOIN Track_File T ON T.recording_id=R.recording_id \
        WHERE T.track_file_id=?', [tfID])
    [recordingPath, recordingID, trackPath] = c.fetchone()  # rst is a string
    con.close()

    print(f'Request for {request.path}, {request.args}, {[recordingPath, recordingID, trackPath]}')

    rai = record_and_identify(recordingPath, recordingID)
    rai.adust_length2(trackPath, start, stop, tfID)
    return ('', 204)

@app.route('/api/addUser')
def CreateUser():
    name = request.args.get("name")
    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    sql='INSERT INTO User (u_name) VALUES (?)'
    c.execute(sql,(name,))
    con.commit()
    con.close()
    return ('', 204)

@app.route('/api/addPlaylist')
def CreatePlaylist():
    name = request.args.get("name")
    uid = int(request.args.get("uid"))
    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    sql='INSERT INTO Playlist (playlist_name,u_id) VALUES (?,?)'
    c.execute(sql,(name,uid))
    con.commit()
    con.close()
    return ('', 204)

@app.route('/api/addTrackToPlaylist')
def AddTrackToPlaylist():
    pid = request.args.get("pid")
    tid = int(request.args.get("tid"))
    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    sql = 'INSERT INTO Playlist_Track(time_added, playlist_id, track_id) VALUES (?,?,?)'
    c.execute(sql,(datetime.now().isoformat(),pid,tid))
    con.commit()
    con.close()
    return ('', 204)

@app.route('/api/removePlaylist')
def RemovePlaylist():
    pid = request.args.get("pid")
    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    sql = 'DELETE FROM Playlist_Track WHERE playlist_id=(?)'
    c.execute(sql, (pid,))
    sql = 'DELETE FROM Playlist WHERE playlist_id=(?)'
    c.execute(sql, (pid,))
    con.commit()
    con.close()
    return ('', 204)

# We might want to add time added so it doens't remove all copies of a song
@app.route('/api/removeTrackFromPlaylist')
def RemoveTrackFromPlaylist():
    pid = request.args.get("pid")
    tid = int(request.args.get("tid"))
    time = request.args.get("time")
    con = sqlite3.connect(DB_PATH)
    c = con.cursor()
    sql = 'DELETE FROM Playlist_Track WHERE playlist_id=(?) AND track_id=(?) AND time_added=(?)'
    c.execute(sql, ( pid, tid, time))
    con.commit()
    con.close()
    return ('', 204)



@app.route('/test')
def albums():
    return {"Test": [1, 2, 3]}

# TODO https://stackoverflow.com/a/45634550/12670653
# This would remove the need to build the frontend with homepage=app
@app.route('/home', defaults={'path': ''})
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    print(f"You want path: {path}")
    # return send_from_directory('build',path)
    return app.send_static_file("index.html")

# https://flask.palletsprojects.com/en/2.0.x/blueprints/
assets = Blueprint('assets', __name__, static_folder='../static', static_url_path='/static')

# @assets.before_request
# def check_url():
#     print("hello")
#     print(request.path)
#     print(request.args)

# @assets.route('')
# def rawArt()

app.register_blueprint(assets)

if __name__ == '__main__':
    app.run(debug=True)
    # getArtistData()
    # getSongData()
