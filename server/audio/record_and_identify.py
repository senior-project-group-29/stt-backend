# import cur as cur
# import keyboard
# import pyaudio
import json
import requests
import shutil
import sqlite3
import time
import os
import wave

from mutagen.flac import FLAC, Picture
from pipes import Template
from pydub import AudioSegment

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
DB_PATH = os.path.join(BASE_DIR, 'stt.db')

class record_and_identify:

    def __init__(self, recordingFileName=None, recordingID=None):
        # print(f"{BASE_DIR}\n{name}\n")
        self.wavF = os.path.join(BASE_DIR,"static","recordings",recordingFileName)        
        self.recording_id = recordingID
        self.jsonD = None
        self.con = None
        self.cur = None
        self.newdir = None

    def record(self,audioFile=None):
        if audioFile:
            CHUNK = 1024
            FORMAT = pyaudio.paInt32
            CHANNELS = 2
            RATE = 44100
            RECORD_SECONDS = 240
            timestr = time.strftime("%Y%m%d-%H%M%S")
            self.wavF = "temp_"+timestr+".wav"


            p = pyaudio.PyAudio()

            print(p.get_default_input_device_info())

            stream = p.open(format=FORMAT,
                            channels=CHANNELS,
                            rate=RATE,
                            input=True,
                            frames_per_buffer=CHUNK)

            print("* recording")

            frames = []
            while True:
                if keyboard.is_pressed('s'):
                    break

            while True:
                print('Recording started: Press S to stop')
                if keyboard.is_pressed('s'):
                    print('Recording stopped')
                    break
                data = stream.read(CHUNK)
                frames.append(data)



            stream.stop_stream()
            stream.close()
            p.terminate()

            wf = wave.open(self.wavF, 'wb')
            wf.setnchannels(CHANNELS)
            wf.setsampwidth(p.get_sample_size(FORMAT))
            wf.setframerate(RATE)
            wf.writeframes(b''.join(frames))
            wf.close()
        else:
            self.wavF=audioFile

    def identify(self):
        '''
        Sets self.jsonD
        '''
        TEMPFILE = 'temp.wav'
        timestr = time.strftime("%Y%m%d-%H%M%S")
        full_file = AudioSegment.from_wav(self.wavF)
        ten_sec = full_file[20000:40000]
        ten_sec.export(TEMPFILE, format="wav")
        f = open(TEMPFILE, "rb")

        data = {
            'api_token': '11b7271b3664bc244e436fb3444c38c2',
            'return': 'lyrics,musicbrainz,spotify,napster,deezer,apple_music',
        }
        files = {
            'file': f,
        }

        result = requests.post('https://api.audd.io/', data=data, files=files)
        f.close()
        os.remove(TEMPFILE)
        # print(result.text)
        # fn = "api_resp" + timestr + ".json"
        # file = open(fn, 'w')
        # file.write(result.text)
        # self.jsonF=fn

        self.jsonD = json.loads(result.text)
        print("START HERE")
        print(self.jsonD)
        print("END HERE")

    def AddMeta(self):
        file = open(self.jsonF, 'r')
        js = json.loads(file.read())
        artist = js['result']['artist']
        title = js['result']['title']
        album = js['result']['deezer']['album']['title']
        release_date = js['result']['deezer']['release_date']
        treack_num = js['result']['deezer']['track_position']
        art = js['result']['deezer']['album']['cover_big']

        resp = requests.get(art)
        file = open("album.jpg", "wb")
        file.write(resp.content)
        file.close()
        audio_file = FLAC('01 - Cluster One.flac')
        audio_file2 = FLAC('flactest.flac20220218-124647.flac')
        print(audio_file.pprint())
        print()
        print(audio_file2.pprint())
        audio_file2['TITLE'] = title
        audio_file2['Album'] = album
        audio_file2['Artist'] = artist
        audio_file2['ALBUMARTIST'] = artist
        audio_file2['DATE'] = str(release_date[0:4])
        audio_file2['TRACKNUMBER'] = str(treack_num)
        ca = Picture()
        ca.type = 3
        ca.desc = "Front Cover"
        imgdata = open('album.jpg', 'rb')

        ca.data = imgdata.read()
        ca.mime = 'image/jpg'
        ca.width = 500
        ca.height = 500
        ca.depth = 24
        audio_file2.add_picture(ca)
        print(audio_file2.pprint())
        audio_file2.save()

    def getOrCreateArtistID(self, artist_name):
        '''
        - Takes a Name, assumes self.con/cur is open
        - Search DB for existing entry
        - If not, create it
        - Return unique artist ID
        '''
        if not self.con:
            self.con = sqlite3.connect(DB_PATH)
        if not self.cur:
            self.cur = self.con.cursor()

        # Search for name
        self.cur.execute(f'SELECT artist_id FROM Artist WHERE artist_name=?', [artist_name])
        rst = self.cur.fetchone()

        # If found, return ID
        if rst:
            print(artist_name, "- Found existing id,", rst[0])
            return rst[0]
        
        # If not, create new entry and return new autoinc id
        sql = "INSERT INTO Artist (artist_name) VALUES (?)"
        self.cur.execute(sql,[artist_name])
        self.con.commit()
        self.cur.execute(f'SELECT artist_id FROM Artist WHERE artist_name=?', [artist_name])
        rst = self.cur.fetchone()[0]
        print(artist_name, "- Created new id,", rst)

        self.con.commit()

        return rst

    def CheckAlbumID(self, title,artist_id):
        if not self.con:
            self.con = sqlite3.connect(DB_PATH)
        if not self.cur:
            self.cur = self.con.cursor()

        # Search for name
        self.cur.execute(f'SELECT album_id FROM Album WHERE title=? AND artist_id=?', [title,artist_id])
        rst = self.cur.fetchone()

        # If found, return ID
        if rst:
            return -1

        else:
            self.cur.execute(f'SELECT MAX(album_id) FROM Album ')
            rst=self.cur.fetchone()
            if rst:
                return int(rst[0])+1
            else:
                return 1


    def split(self):
        '''
        - Inserts Artist Data
        - Inserts Album Data
        - Splits Track_Files
        - Inserts Track Data 
        '''
        self.con = sqlite3.connect(DB_PATH)
        self.cur = self.con.cursor()

        audio_seg = AudioSegment.from_wav(self.wavF)
        # file = open(self.jsonF, 'r')
        # js = json.loads(file.read())
        js = self.jsonD
        
        album_id = 4 # FIXME
        album_title = js['result']['deezer']['album']['title']
        release_date = js['result']['deezer']['release_date']
        art_url = js['result']['deezer']['album']['cover_big']
        # artist_id = 3 # FIXME
        artist_name = js['result']['artist']
        artist_id = self.getOrCreateArtistID(artist_name)
        album_id=self.CheckAlbumID(album_title,artist_id)
            
        if album_id<0:
            return

        
        self.newdir = os.path.join(BASE_DIR, "static", "albums", album_title)
        try:
            os.mkdir(self.newdir)
        except:
            pass
        # newdir = "./%s" % (album_title)
        # newdir=newdir1.replace(' ','_')
        # shutil.move('album.jpg', "./" + newdir)

        ## ART
        respa = requests.get(art_url)
        art_file_path = os.path.join(self.newdir, "album.jpg")
        art_file_path_rel = os.path.join("albums", album_title, "album.jpg")
        art_file = open(art_file_path, "wb")
        art_file.write(respa.content)
        art_file.close()

        ## ART Flac File Stuff
        ca = Picture()
        ca.type = 3
        ca.desc = "Front Cover"
        ca.mime = 'image/jpg'
        ca.width = 500
        ca.height = 500
        ca.depth = 24
        imgdata = open(art_file_path, 'rb')
        ca.data = imgdata.read()
        imgdata.close()

        ## TRACKS
        print("Tracklist\n\n", js['result']['deezer']['album']['tracklist'])
        resp = requests.get(js['result']['deezer']['album']['tracklist'])
        # print(resp.text)
        js2 = json.loads(resp.text)
        print(len(js2['data']))
        dl = [0] # ascending list of total time (added per track)
        nl = [] # track names
        ids = [] # unique ids

        ## for each track in the album
        for i in range(0, len(js2['data'])):
            duration = js2['data'][i]['duration']
            title = js2['data'][i]['title']
            track_id = js2['data'][i]['id']

            print(title)
            print(duration)

            dl.append(dl[i] + duration) # runtime total durations
            nl.append(title)
            ids.append(track_id)

        sql = "INSERT INTO Album (album_id,title,a_length,cover_art,artist_id) VALUES (?,?,?,?,?)"
        self.cur.execute(sql,(album_id,album_title,int(dl[-1]),art_file_path_rel,artist_id))

        sql = f'UPDATE Recording_File SET album_id="{album_id}" WHERE recording_id="{self.recording_id}"'
        print("\n\n>>>>>>>>>>>>>>>>>>>\n"+sql)
        # self.cur.execute(sql,(album_id))
        self.cur.execute(sql)

        ## for each track duration, first track duration is 0
        for i in range(0, len(dl) - 1):
            track_num = i + 1
            track_title = nl[i]
            track_path = os.path.join(self.newdir, track_title + '.flac')
            track_path_rel = os.path.join("albums", album_title, track_title + '.flac')
            track_length = int(-dl[i] + dl[track_num])
            # print(dl[i])
            # print(dl[i + 1])
            
            rt = audio_seg[dl[i] * 1000:dl[track_num] * 1000]
            # rt.export(newdir+"/" + track_title + '.flac', format="flac")
            rt.export(track_path, format="flac")

            flac_file_inst = FLAC(track_path)
            flac_file_inst['TITLE'] = track_title
            flac_file_inst['Album'] = album_title
            flac_file_inst['Artist'] = artist_name
            flac_file_inst['ALBUMARTIST'] = artist_name
            flac_file_inst['DATE'] = str(release_date[0:4])
            flac_file_inst['TRACKNUMBER'] = str(track_num)
            flac_file_inst['COMMENT'] = str(dl[i] * 1000)+':'+str(dl[i + 1] * 1000)

            flac_file_inst.add_picture(ca)
            flac_file_inst.save()

            sql='INSERT INTO Track (track_id,track_name,track_length,recorded_by,track_number,album_id) VALUES (?,?,?,?,?,?)'
            self.cur.execute(sql,(ids[i],track_title,track_length,artist_name,track_num,album_id))
            sql='INSERT INTO Track_File (track_file_id, f_name, start_time, stop_time, track_id, recording_id)  VALUES (?,?,?,?,?,?)'
            self.cur.execute(sql,(ids[i], track_path_rel, dl[i] * 1000, dl[i + 1] * 1000, ids[i], self.recording_id))

        self.con.commit()
        self.con.close()

    def dodql(self):
        self.con = sqlite3.connect("test.db")
        self.cur = self.con.cursor()
        self.cur.executescript('delete from Track where NOT track_id=0  ')
        self.cur.executescript('delete from Album where NOT album_id=0  ')
        '''file= open('schema.sql','r')
        s=''
        for line in file:
            s+=line

        print(s)
        self.cur.executescript(s)'''
    def T(self):
        self.cur.execute('SELECT * from Track')
        print(self.cur.fetchall())

    def manual_split(self):
        test = AudioSegment.from_wav(self.wavF)
        album = input('Enter Album Name')
        artist = input('Enter Artist Name')
        release_date = input('Enter Release Year')
        numTracks = input('Enter Number of Tracks')
        name_lengths = []
        start=0
        self.newdir = "./%s" % (album)
        # newdir=newdir1.replace(' ','_')
        try:
            os.mkdir(self.newdir)
            shutil.move('album.jpg', "./" + self.newdir)
        except:
            pass

        for i in range(0,int(numTracks)):
            prompt = "Enter Title For Track Number "+str(i+1)
            tname = input(prompt)
            prompt2 = "Enter length of track "+str(tname)+" in Seconds"
            tlength = input(prompt2)
            name_lengths.append([tname,int(tlength)])
        for i in range(0, len(name_lengths) ):


            rt = test[start:name_lengths[i][1] * 1000]
            start+=name_lengths[i][1]
            rt.export(self.newdir+"/" + name_lengths[i][0] + '.flac', format="flac")
            writeTage = FLAC(self.newdir+"/"  + name_lengths[i][0] + '.flac')
            writeTage['TITLE'] = name_lengths[i][0]
            writeTage['Album'] = album
            writeTage['Artist'] = artist
            writeTage['ALBUMARTIST'] = artist
            writeTage['DATE'] = str(release_date)
            writeTage['TRACKNUMBER'] = str(i+1)
            writeTage['COMMENTS']=str(start)+':'+str(name_lengths[i][1] * 1000)
            writeTage.save()

    def remove_whitespace(self):
        test = AudioSegment.from_wav(self.wavF)
        wlen = input('Enter Length Of White Space At Start In Seconds')
        rt = test[int(wlen)*1000:]
        fname = self.wavF.split('/')
        rt.export('static/recordings/trimmed_'+fname[-1],format='wav')
        self.wavF='static/recordings/trimmed_'+fname[-1]

    def adust_length(self):
        test = AudioSegment.from_wav(self.wavF)
        flist = os.listdir(self.newdir)
        tlist=[]
        x = 1
        for i in flist:
            print(i[-5:])
            if i[-5:]=='.flac':
                print('Track #'+str(x)+': '+i)
                tlist.append(i)
                x+=1
        edit = input('Select What Track to Edit')
        pls = FLAC(self.newdir+'/'+tlist[int(edit)-1])
        print(pls.pprint())
        print(pls['COMMENT'])
        length = pls['COMMENT'][0].split(':')
        title = pls['TITLE']
        album = pls['Album']
        artist = pls['Artist']
        albumartist = pls['ALBUMARTIST']
        date = pls['DATE']
        tracknum = pls['TRACKNUMBER']
        ca = Picture()
        ca.type = 3
        ca.desc = "Front Cover"
        imgdata = open(self.newdir + '/album.jpg', 'rb')
        ca.data = imgdata.read()
        ca.mime = 'image/jpg'
        ca.width = 500
        ca.height = 500
        ca.depth = 24
        print(length)
        pls.save()
        front = input('Length In Seconds To Add Or Subtract From Front Of Track')
        back = input('Length In Seconds To Add Or Subtract From Back Of Track')
        newLength = [int(length[0])-int(front)*1000,int(back)*1000+int(length[1])]
        print(newLength)
        print(self.newdir + "/" + tlist[int(edit)-1])
        rt = test[newLength[0]:newLength[1]]
        rt.export(self.newdir + "/" + tlist[int(edit)-1], format="flac")
        newMeta = FLAC(self.newdir+'/'+tlist[int(edit)-1])
        newMeta['TITLE'] = title
        newMeta['Album'] = album
        newMeta['Artist'] = artist
        newMeta['ALBUMARTIST'] = albumartist
        newMeta['DATE'] = date
        newMeta['TRACKNUMBER'] = tracknum
        newMeta['COMMENT'] = str(newLength[0])+':'+str(newLength[1])
        newMeta.add_picture(ca)
        newMeta.save()

    def adust_length2(self,trackPath,start,stop,tfID):
        self.con = sqlite3.connect(DB_PATH)
        self.cur = self.con.cursor()

        originalRecording:AudioSegment = AudioSegment.from_wav(self.wavF)
        trackPath = os.path.join("static", trackPath)
        albumArtPath = os.path.join(os.path.dirname(trackPath), "album.jpg")
        print(albumArtPath)

        # flist = os.listdir(trackPath)
        tlist=[]
        x = 1

        pls = FLAC(trackPath)
        length = pls['COMMENT'][0].split(':')
        title = pls['TITLE']
        album = pls['Album']
        artist = pls['Artist']
        albumartist = pls['ALBUMARTIST']
        date = pls['DATE']
        tracknum = pls['TRACKNUMBER']
        ca = Picture()
        ca.type = 3
        ca.desc = "Front Cover"
        imgdata = open(albumArtPath, 'rb')
        ca.data = imgdata.read()
        ca.mime = 'image/jpg'
        ca.width = 500
        ca.height = 500
        ca.depth = 24
        print("Old start, stop: ", length)
        pls.save()

        # MATH!
        # newLength = [int(length[0])-int(frontOffset)*1000,int(backOffset)*1000+int(length[1])]
        rt:AudioSegment = originalRecording[start:stop]
        rt.export(trackPath, format="flac")
        newMeta = FLAC(trackPath)
        newMeta['TITLE'] = title
        newMeta['Album'] = album
        newMeta['Artist'] = artist
        newMeta['ALBUMARTIST'] = albumartist
        newMeta['DATE'] = date
        newMeta['TRACKNUMBER'] = tracknum
        newMeta['COMMENT'] = f'{start}:{stop}'
        newMeta.add_picture(ca)
        newMeta.save()

        sql = "UPDATE Track_File SET start_time=?, stop_time=? WHERE track_file_id=?" 
        self.cur.execute(sql, [start,stop,tfID])
        sql = "UPDATE Track SET track_length=? WHERE track_id=?"
        self.cur.execute(sql, [(stop-start)/1000,tfID])
        print("New start, stop: ", [stop, start])
        print("New length: ", (stop - start)/1000)
        self.con.commit()
        self.con.close()


def EditMeta(TrackPath,MetaType,NewMeta):
    newMeta = FLAC(TrackPath)
    if MetaType == 'Title':
        newMeta['TITLE'] = NewMeta
    elif MetaType=='Album':
        newMeta['Album'] = NewMeta
    elif MetaType=='Artist':
        newMeta['Artist'] = NewMeta
    elif MetaType=='ALBUMARTIST':
        newMeta['ALBUMARTIST'] = NewMeta
    elif MetaType=='DATE':
        newMeta['DATE'] = NewMeta
    elif MetaType=='TRACKNUMBER':
        newMeta['TRACKNUMBER'] = NewMeta
    newMeta.save()

if __name__ == '__main__':
    SampleOne = record_and_identify("Amorphis_test.wav", 1)
    SampleOne.con = sqlite3.connect(DB_PATH)
    SampleOne.cur = self.con.cursor()

    # SampleOne.record('static/recordings/Amorphis_test.wav')
    # SampleOne.dodql()
    # SampleOne.identify()
    # SampleOne.split()
    # SampleOne.T()
    SampleOne.getOrCreateArtistID("Lil Nas X")

    SampleOne.con.close()

