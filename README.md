# STT Backend

Backend application running on the STT-1.

## Description

Database and backend server

## Installation

> Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

> Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

> Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

> If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

How to get running after cloning this repo.

### Prerequisites

- sqlite3 (version >= 3.33.0)
  - Ubuntu 20.04 only has 3.31 in the focal repos
- python
- pip

### Setup

1. Python virtual environment
   - ```console
     $ python3 -m venv env # create new venv in ./env
     $ source env/bin/activate # activate venv
     ```
1. Install dependencies
   - Run:
     ```console
     (env)$ pip install -r requirements-freeze.txt
     ```
   - For `pyaudio` to install correctly, I needed the following system packages installed:
     - `portaudio19-dev`
1. Database
   - Run `schema.sql` with `sqlite3`. This will:
     - to build database, `stt.db`
     - fill it with data from `/data` csv files.
     ```console
     $ sqlite3 -init schema.sql stt.db
     ```
   - SQLite CLI commands:
     ```sql
     .exit -- exit the CLI
     .import ./data/album.csv album -- ingest csv into a table
     .read ./queries.sql -- run a sql file
     ```
   - Enter back into the CLI with
     ```console
     $ sqlite3 stt.db
     ```

## Authors and acknowledgment

- Wayne Fairbrother
- Kyle Novak
- Michael Palumbo
- Max Prehl
- Jake Proctor
- Adam Torcomian

## License

All rights reserved, the above authors. 2022.
