-- STT-DB Queries 
--
-- Example accesses to our database

.mode json

SELECT A.title, A.a_date, R.artist_name, L.label_name
-- SELECT A.title, A.a_date, R.artist_name, L.label_name, S.studio_name
FROM Album A
JOIN Artist R on A.artist_id=R.artist_id
JOIN Label L on A.label_id=L.label_id
-- JOIN Studio S on A.studio_id=S.studio_id
;

-- Add cover art file path to existing entry
UPDATE Album
SET cover_art='1'
WHERE album_id=1
;
